#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SLPPlayer.generated.h"

UENUM()
enum P1Ability
{
	None 		UMETA(DisplayName = "None"),
	Fly  	 	UMETA(DisplayName = "Moving"),
	Teleport    UMETA(DisplayName = "Teleport"),
	DoubleJump  UMETA(DisplayName = "DoubleJump")
};

UCLASS()
class SLP_API ASLPPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASLPPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
    // Movement functions, bound to directional keys
	void MoveForward(float Value);
	void MoveRight(float Value);
	
	// Look functions, used to aim with the mouse or a controller
	void LookHorizontal(float Value);
	void LookVertical(float Value);

    // Called on the server to enable an ability
	UFUNCTION(Server, Reliable)
	void ServerP2EnableAbility(int Ability);

	// Runs on the server, updates the current selected ability on all clients
	UFUNCTION(NetMulticast, Reliable)
	void MulticastP2EnableAbility(int Ability);
	
	// Wraps P2EnableAbility, can be safely called by either player
	void TryEnableAbility(int Ability);

	// Should only be called by player two, doesn't handle networking
	// Don't call this directly, use a wrapper function
	void P2EnableAbility(int Ability);
	DECLARE_DELEGATE_OneParam(FEnableAbility, int Ability);

	// Mainly used for teleportation, called by clicking the left mouse button
	void Interact();

	// Register player 2's special capabilities with the server
	UFUNCTION(Server, Reliable)
	void ServerStartP2();

	// Register player 2's special capabilities with the clients
	UFUNCTION(NetMulticast, Reliable)
	void MulticastStartP2();

	// Custom jump functions that activate abilities if they are enabled
	void CustomJump();
	void JumpReleased();

	float GravityScale = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite)
	float Sensitivity = 1.0f;

	UPROPERTY(BlueprintReadWrite)
	bool IsP2 = false;

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<P1Ability> CurrentP1Ability = None;
};
