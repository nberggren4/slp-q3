#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * Used for testing 
 */
UCLASS()
class SLP_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
