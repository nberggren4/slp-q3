// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SLPGameModeBase.generated.h"

/**
 * Derived from by gamemode blueprints in the editor
 */
UCLASS()
class SLP_API ASLPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
