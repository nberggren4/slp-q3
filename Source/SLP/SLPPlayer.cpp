#include "SLPPlayer.h"

#include "DrawDebugHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Containers/UnrealString.h"

// The current ability needs to be stored on both players,
// given the way that UE4's RPCs work, this is the best way
// to do it without making significant modifications to the
// gamemode class and causing ability logic to be more complex
static P1Ability GCurrentP1Ability = None;

// Sets default values
ASLPPlayer::ASLPPlayer()
{
	// The character will call tick every frame
	PrimaryActorTick.bCanEverTick = true;
	// Tell UE4 that this character should sync over the network
	bReplicates = true;
	// Improve platforming by letting the player change directions in the air
	GetCharacterMovement()->AirControl = 1;
	GravityScale = GetCharacterMovement()->GravityScale;
}

// Called when the game starts or when spawned
void ASLPPlayer::BeginPlay()
{
	Super::BeginPlay();

	// Player 1 is the host, if this character is not the host, it must be player 2
	if (!HasAuthority())
	{
		GEngine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("P2 attributes enabled!"));
		IsP2 = true;
		ServerStartP2();
	}
}

// Propogate IsP2 to the server
void ASLPPlayer::ServerStartP2_Implementation()
{
	IsP2 = true;
}

// Propogate IsP2 to the client
void ASLPPlayer::MulticastStartP2_Implementation()
{
	IsP2 = true;
}

/*
 * The following methods are all wrappers over ACharacter's methods, and are called by peripheral inputs
 */

void ASLPPlayer::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector(), Value);
}

void ASLPPlayer::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector(), Value);
}

void ASLPPlayer::LookHorizontal(float Value)
{
	AddControllerYawInput(Value * Sensitivity);
}

void ASLPPlayer::LookVertical(float Value)
{
	AddControllerPitchInput(-Value * Sensitivity);
}

// Enable an ability, only if the player calling is player two
void ASLPPlayer::TryEnableAbility(int Ability)
{
	if (!IsP2) return;
	
	ServerP2EnableAbility(Ability);
}

// Change the currently selected ability, this is the end result of
// every other "enable ability" method
void ASLPPlayer::P2EnableAbility(int Ability)
{
	if (!IsP2) return;
	
	GCurrentP1Ability = static_cast<P1Ability>(Ability);
}

void ASLPPlayer::ServerP2EnableAbility_Implementation(int Ability)
{
	P2EnableAbility(Ability);
	MulticastP2EnableAbility(Ability);
}

void ASLPPlayer::MulticastP2EnableAbility_Implementation(int Ability)
{
	P2EnableAbility(Ability);
}

// Called every frame
void ASLPPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentP1Ability = GCurrentP1Ability;
}

void ASLPPlayer::CustomJump()
{
	if (!IsP2) // Player two only has a normal jump available
	{
		JumpMaxCount = 1; // Assume jump count is one
		
		switch (GCurrentP1Ability)
		{
		case Fly:
			// Change the properties of the character to allow for flight/hovering
			GEngine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Starting to fly!"));
			GetCharacterMovement()->GravityScale = 0;
			GetCharacterMovement()->AirControl = 0.0f;
			break;
		case DoubleJump:
			// The previous assumption was wrong, because double jump
			// is enabled
			JumpMaxCount = 2;
			break;
		default:
			break;
		}
	}
	
	Jump();
}

void ASLPPlayer::JumpReleased()
{
	// Reset properties to default, this is useful in case
	// these properties were previously modified by the player
	// beginning to fly
	GetCharacterMovement()->GravityScale = GravityScale;
	GetCharacterMovement()->AirControl = 1;
}

void ASLPPlayer::Interact()
{
	if (IsP2) return; // Player two doesn't have any special interactions
	
	if (GCurrentP1Ability == Teleport)
	{
		// Get the current camera for player one
		APlayerCameraManager *CamManager = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
		
		// Create a result which will store information about a future raycast
		FHitResult OutHit;
		// The raycast should start from the player's camera
		FVector Start = CamManager->GetCameraLocation();

		// The raycast should be aimed where the player is looking
		FVector ForwardVector = CamManager->GetCameraRotation().Vector();
		// The raycast should only travel 1000 units, which isn't much in UE4
		// this helps to balance the power of teleportation
		FVector End = ForwardVector * 1000.f + Start;
		FCollisionQueryParams CollisionParams;

		// Draw a line to show where the teleportation raycast will travel
		DrawDebugLine(GetWorld(), Start, End, FColor::Purple, false, 10, 0, 1);

		// Fire a raycast, continue if anything visible is collided with
		if(GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams)) 
		{
			// Ignore non-blocking hits, we only want to collide with solid objects
			if(OutHit.bBlockingHit)
			{	
				// Teleport to a teleport point, if that is what the player hit
				if (OutHit.GetActor()->GetName().Contains("TeleportPoint"))
				{
					GEngine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("P1 teleporting!"));
					SetActorLocation(OutHit.GetActor()->GetActorLocation());
				} // If the player hit a level finish portal, travel to the next level
				else if (OutHit.GetActor()->GetName().Contains("Level1Finish"))
				{
					GetWorld()->ServerTravel("/Game/GameContent/Level2?Listen", true);
				}
				else if (OutHit.GetActor()->GetName().Contains("Level2Finish"))
				{
					GetWorld()->ServerTravel("/Game/GameContent/MainMenu", true);
				}
			}
		}
	}
}

// Called to bind functionality to input
void ASLPPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	/* Bind Unreal Engine inputs to the methods in the rest of the class */

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASLPPlayer::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASLPPlayer::MoveRight);
	PlayerInputComponent->BindAxis("LookHorizontal", this, &ASLPPlayer::LookHorizontal);
	PlayerInputComponent->BindAxis("LookVertical", this, &ASLPPlayer::LookVertical);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASLPPlayer::CustomJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ASLPPlayer::JumpReleased);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ASLPPlayer::Interact);

	PlayerInputComponent->BindAction<FEnableAbility>("P2EnableFly", IE_Pressed, this, &ASLPPlayer::TryEnableAbility, static_cast<int>(Fly));
	PlayerInputComponent->BindAction<FEnableAbility>("P2EnableDoubleJump", IE_Pressed, this, &ASLPPlayer::TryEnableAbility, static_cast<int>(DoubleJump));
	PlayerInputComponent->BindAction<FEnableAbility>("P2EnableTeleport", IE_Pressed, this, &ASLPPlayer::TryEnableAbility, static_cast<int>(Teleport));
	PlayerInputComponent->BindAction<FEnableAbility>("P2EnableNone", IE_Pressed, this, &ASLPPlayer::TryEnableAbility, static_cast<int>(None));
}

