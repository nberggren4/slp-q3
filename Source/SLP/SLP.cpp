// Copyright Epic Games, Inc. All Rights Reserved.

#include "SLP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SLP, "SLP" );
